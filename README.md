This repo holds the generated site for ipfs, and rehosts it on gitlab pages for redundancy and debugging 

Other than holding files statically and a single run of npm's all-relative, this should be the same site as ipfs.io/ipns/tastytreasures.eth

Hosted here: http://petroff.ryan.gitlab.io/static-hosted-site-tastytreasures/

Note: https doesn't work for gitlab reasons, http is fine.
